This project aims to implement a sequence of tasks based on a question-and-answer based website. The work is still in progress and part-1 and part-2 have been uploaded so far.

## Tasks

    Users can ask questions
    Each Question can have many answers


## Tasks Part-1(Create API's)

    User Sign up(new user)
    User Login
    User Logout
    View all users
    Change password
    Delete a user account

    Browse all questions and users
    View a question
    View all questions by posted by a user
    Post a question              
    Edit a question     
    Delete a question  

## Tasks Part-2(Integrate API's to frontend)
    url for home page
    url for login
    url for delete
    url for  modify password
    url for user home page
    functions to display questions and answers
    add functionality like add question, modify question, delete question, add answer, delete answer, modify answer
                  

## Technologies Used:

    Python  
    Flask   
    SQLAlchemy
    Elastic Search  
    SQLite  
    Postman 

## Run the program:
    Install python 3.7.
    Run in terminal:
      pip install -r requirements.txt
    Read documentation for how to create url for getting the response.
    Run in terminal:
      python3 run.py
    Do API hits using Postman.


## Directory Structure:    
    
        .
    ├── application
    │   ├── answers.py
    │   ├── config.ini
    │   ├── forms.py
    │   ├── __init__.py
    │   ├── models.py
    │   ├── project.db
    │   ├── questions.py
    │   ├── routes.py
    │   ├── search.py
    │   ├── templates
    │   │   ├── answers.html
    │   │   ├── base.html
    │   │   ├── home.html
    │   │   ├── login.html
    │   │   ├── modify-answer.html
    │   │   ├── modify_user.html
    │   │   ├── post-ans.html
    │   │   ├── question-modify.html
    │   │   ├── search-result.html
    │   │   ├── signup.html
    │   │   ├── user-home.html
    │   │   └── users.html
    │   └── views.py
    ├── documentation.pdf
    ├── flask api.postman_collection_1.json
    ├── README.md
    ├── requirement.txt
    └── run.py




## Database Documentation

Schema for Database:

User - Table :

    user_id     --- int            --- primary key(auto incremented)
    username    --- string(20)     --- unique value
    email       --- string(120)     --- unique value
    password    --- string(60)     --- hashed password


Questions - Table :

    id          --- int            --- primary key(auto incremented)
    user_id     --- int            --- foreign key(referencing user table)
    title       --- string(100)    --- 
    question    --- text    --- 

Answer - Table :

    answer_id       --- int            --- primary key(auto incremented)
    answer_desc     --- int            --- 
    user_id       --- string(100)    --- foreign key(referencing use table)
    question_id    --- text    --- foreign key(referencing question table)



          
      
    
