from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, TextAreaField
from wtforms.validators import DataRequired, Email, ValidationError
from application.models import User
from flask import flash


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(),Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Sign In')

class SignUpForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', validators=[DataRequired()])
    submit = SubmitField('Sign Up')

    def validate_username(self, username):
        user = User.query.filter(User.username==username.data).first()
        if user:
            flash('Username already in use')
            raise ValidationError('Username already exists')

    def validate_email(self, email):
        user = User.query.filter(User.email==email.data).first()
        if user:
            flash('email already in use')
            raise ValidationError('Email already exists')

class QuestionForm(FlaskForm):
    title = StringField('Question Title', validators=[DataRequired()])
    description = TextAreaField('Description', validators=[DataRequired()])
    submit = SubmitField('POST')

class ModifyForm(FlaskForm):
    title = StringField('Question Title', validators=[DataRequired()])
    description = StringField('Description', validators=[DataRequired()])
    submit = SubmitField('Modify')


class ChangePwdForm(FlaskForm):
    old_password = PasswordField('Current Password', validators=[DataRequired()])
    new_password = PasswordField('New Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', validators=[DataRequired()])
    submit = SubmitField('Modify')

class AnswerForm(FlaskForm):
    answer = StringField('Post Answer', validators=[DataRequired()])
    submit = SubmitField('POST')

class ModifyAnsForm(FlaskForm):
    answer_desc = StringField('New Answer', validators=[DataRequired()])
    submit = SubmitField('Modify')




