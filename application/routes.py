"""routes for user account handling are defined"""
from application.models import User, Question, Answers
from flask import url_for, flash, redirect, request, abort, jsonify,Flask
from application import app, db, bcrypt,login_manager,jwt, blacklist
from flask_login import login_user, current_user, logout_user, login_required
from flask_jwt_extended import create_access_token,jwt_required,get_jwt_identity,get_raw_jwt
import datetime
from application.celery_background_jobs import download_csv_data


@login_manager.user_loader
def load_user(user_id):
    return User.get(user_id)


@app.route("/user", methods=['GET', 'POST'])
def register():
    """
    to get all users and for new user registration

    :http relative-url: /user
    :http body contents : username, email, and password(json format), if POST method
    :http-method : GET/POST
    """
    request_json = request.json
    if request.method=='POST':
        pwd = request_json["password"]
        confirm_pwd = request_json["confirm password"]
        if pwd!=confirm_pwd:
            return {"Error":"Password and Confirm password don't match"}, 422
            
        else:
            hashed_password = bcrypt.generate_password_hash(pwd).decode('utf-8')
            user = User(username=request_json["username"], email=request_json["email"], password=hashed_password)
            db.session.add(user)
            db.session.commit()
            return {"message":"Account created successfully"}, 200

    elif request.method=='GET':
        users = User.query.all()
        if users:
            dict_users = {}
            for each_user in users:
                dict_users[each_user.user_id]={"id": each_user.user_id,\
                    'username':each_user.username, 'email': each_user.email}
            return dict_users
        else:
            return {"Message": "No accounts found"}, 404 

@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    "check if token is blacklisted"

    jti = decrypted_token['jti']
    return jti in blacklist

@app.route('/login', methods=['POST'])
def login():
    """
    User login function.

    :http relative-url: /signin
    :http body contents : username, password(json format).
    :http-method :POST
    """

    credentials = request.json
    user = User.query.filter_by(email=credentials['email']).first()
    if user is None:
         return {"Error": "Invalid mail id"}, 404
    authorized = bcrypt.check_password_hash(user.password, credentials['password'])
    hashed_password = bcrypt.generate_password_hash(credentials['password']).decode('utf-8')
    if not authorized:
         return {"Error": "Invalid password"}, 404
    expires = datetime.timedelta(days=1)
    access_token = create_access_token(identity=str(user.user_id), expires_delta=expires)
    return {'token': access_token}, 200


@app.route('/user/<int:userid>', methods=['PUT'])
@jwt_required
def modify(userid):
    """
    User login function.

    :http relative-url: /user/<int:userid>
    :http body contents : username, password, confirm password(json format).
    :http-method :PUT
    """
    if str(userid)==get_jwt_identity():
        modify = request.json
        user = User.query.filter_by(user_id=userid).first()
        auth_old_pwd = bcrypt.check_password_hash(user.password, modify['old_password'])
        if not auth_old_pwd:
            return {"Error": "Invalid password"}, 404
        if modify["new_password"]!=modify["confirm_password"]:
            return{"Error":"Password and Confirm Password do not match"}
        new_hash_pwd = bcrypt.generate_password_hash(modify['new_password']).decode('utf-8')
        user.password = new_hash_pwd
        db.session.add(user)
        db.session.commit()
        return {'Message': 'Password updated'}, 200
    else:
        return {'Error': 'Not authorized to perform desired operation'}, 401


@app.route('/user/<int:userid>', methods=['DELETE'])
@jwt_required
def delete(userid):
    """
    Account delete function.

    :http relative-url: /user/<int:userid>
    :http-method :DELETE
    """

    delete_user = User.query.filter_by(user_id=userid).first()
    # delete_
    if delete_user:
        if str(userid)==get_jwt_identity():
            db.session.delete(delete_user)
            db.session.commit()
            return jsonify({'result': True})
        else:
            return {'Error': 'Not authorized to perform desired operation'}, 401
    else:
        return {"message": "Account doesn't exist"}, 404



@app.route('/logout', methods=['DELETE'])
@jwt_required
def logout():
    """
    User logout function.

    :http relative-url: /logout>
    :http-method :DELETE
    """
    jti = get_raw_jwt()['jti']
    blacklist.add(jti)
    return {"message": "Successfully logged out"}, 200

@app.route('/covid-daily-data',methods=["PUT"])
def get_daily_covid_data():
    task = download_csv_data.delay()
    return {'task_id':task.task_id}