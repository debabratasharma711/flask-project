from application import app, celery
import csv
from io import StringIO
from datetime import date, timedelta
import requests


@celery.task(name="tasks.covid_daily_data")
def download_csv_data(index="covid_daily_data"):
    today = date.today() - timedelta(days = 3)
    today = today.strftime("%m-%d-%Y")
    response_text = requests.get("https://raw.githubusercontent.com/CSSEGISan"+\
        "dData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/"\
            +today+".csv").text
    csv_data = StringIO(response_text)
    csv_dict =  csv.DictReader(csv_data,delimiter=',')
    data_list = []
    for row in csv_dict:
        data_list.append(dict(row))
    if app.elasticsearch.indices.exists(index):
        app.elasticsearch.indices.delete(index)
    app.elasticsearch.indices.create(index, body=app.config["MAPPING"])
    for row in data_list:
        app.elasticsearch.index(index=index, body=row)