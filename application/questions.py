"""routes for questions posted by user are defined"""
from flask import url_for, flash, redirect, request,abort, jsonify,Flask
from application import app, db, bcrypt,login_manager
from application.models import User, Question,Answers
from flask_login import login_user, current_user, logout_user, login_required
from flask_jwt_extended import create_access_token,jwt_required, get_jwt_identity
import datetime
import json
from application.search import query_index, get_data_to_add, remove_from_index
# get all question
@app.route('/questions', methods=['GET'])
def get_questions():
    """
    Get question API endpoint function.

    :http relative-url: /questions
    :http-method :GET
    """

    questions = Question.query.all()
    if questions:
        dict_questions={}
        for each_question in questions:
            dict_questions[each_question.q_id]={"q_id": each_question.q_id,\
                                                'title':each_question.title, 'description': each_question.description,\
                                                'user_id': each_question.user_id
                                                }
            
        return jsonify(dict_questions)
    else:
        return {"Message": "No questions posted"}, 404

# get question by question id
@app.route('/questions/<int:qid>', methods=['GET'])
def get_question_by_qid(qid):
    """
    Get question by question id API endpoint function.

    :http relative-url: /questions/<int: qid>
    :http-method :GET
    """

    question = Question.query.filter_by(q_id=qid).first()
    if question:
        dict_question = {}
        dict_question[question.user_id]={"q_id": question.q_id,'title':question.title, 'description': question.description}
        return jsonify(dict_question)
    else:
        return {"Message": "No questions posted"}, 404


# get questions by user id
@app.route('/user/<int:uid>/questions', methods=['GET'])
def get_question_by_uid(uid):
    """
    Get question by user id API endpoint function.

    :http relative-url: user/<int:uid>/questions
    :http-method :GET
    """

    questions = Question.query.filter_by(user_id=uid).all()
    if questions != []:
        dict_questions = {}
        for each_question in questions:
            if each_question.user_id not in dict_questions.keys():
                    dict_questions[each_question.user_id]=[]
                    dict_questions[each_question.user_id].append({"q_id": each_question.q_id,'title':each_question.title, 'description': each_question.description,'user id': each_question.user_id})
            else:    
                dict_questions[each_question.user_id].append({"q_id": each_question.q_id,'title':each_question.title, 'description': each_question.description, 'user id': each_question.user_id})
        return jsonify(dict_questions)
    else:
        return {"message": "No questions posted"}, 404


# post a question
@app.route('/questions', methods=['POST'])
@jwt_required
def post_question():
    """
    Post question API endpoint function.

    :http relative-url: /questions
    :http body contents : title, description(json format).
    :http-method :POST
    """

    api_question = request.json
    user_id = get_jwt_identity()
    question = Question(title=api_question["title"],description=api_question["description"],user_id=int(user_id))
    db.session.add(question)
    db.session.commit()
    get_data_to_add(app.config['INDEX'])
    return {'Message': 'Question added'}, 200

# update a question
@app.route('/questions/<int:qid>', methods=['PUT'])
@jwt_required
def update_question(qid):
    """
    Get question API endpoint function.

    :http relative-url: /questions
    :http body contents : title or description or both(json format).
    :http-method :PUT
    """

    question = Question.query.get(qid)
    if question:
        if str(question.user_id)==get_jwt_identity():
            api_update = request.json
            if "title" in api_update.keys():
                mod_question = Question.query.filter_by(q_id=qid).first()
                mod_question.title = api_update["title"]
            if "description" in api_update.keys():
                mod_question = Question.query.filter_by(q_id=qid).first()
                mod_question.description = api_update["description"]
            db.session.add(mod_question)
            db.session.commit()
            get_data_to_add(app.config['INDEX'])

            return {'Message': 'Modified'}, 200
        else:
            return {"message":"Unauthorized access"},401
    else:
        return {"Message": "no questions with the given id"}, 404

# delete a question
@app.route('/questions/<int:qid>', methods=['DELETE'])
@jwt_required
def del_question(qid):
    """
    Delete question API endpoint function.

    :http relative-url: /questions/<int: qid>
    :http-method : DELETE
    """

    delete_question = Question.query.filter_by(q_id=qid).first()
    if delete_question:
        print("ppppppppppppppppp",delete_question)
        if str(delete_question.user_id)==get_jwt_identity():
            # answers = list(delete_question.answer)
            # print(answers)
            # delete_post = [delete_question]+answers
            # print("delete post\n\n",delete_post)
            # for each_post in delete_post:
            print("xyz")
            db.session.delete(delete_question)
            db.session.commit()
            remove_from_index(app.config['INDEX'], delete_question)
            
            return {'Message': 'Question deleted'}, 200
        else:
            return{'Message':'Unauthorized access'},401
    else:
        return {"Message":"Message doesn't exist"}, 404

@app.route('/search/search_query', methods=['GET'])
def api_search_question():
    search_query = request.json
    print("query:",search_query)
    if search_query:
        q_id, hits = query_index(app.config['INDEX'], search_query['query'], 1, 5)
        if hits>0:
            response = []
            for ids in q_id:
                question = Question.query.filter(Question.q_id==ids).first()
                question_user = User.query.filter(User.user_id==question.user_id).first()
                answers = Answers.query.filter(Answers.question_id==ids, Answers.answer_desc.ilike("%"+search_query['query']+"%")).all()
                if question_user:
                    question_author = question_user.username
                else:
                    question_author = "No author name"
                replies = []
                question_title = question.title
                question_description = question.description
                for each_answer in answers:
                    answer_user = User.query.filter(User.user_id==each_answer.user_id).first()
                    answer_author = answer_user.username 
                    reply = dict()
                    reply["author"] = answer_author
                    reply["answer"] = each_answer.answer_desc
                    replies.append(reply)
                search_result_dict = dict()
                search_result_dict["id"] = ids
                search_result_dict["author"] = question_author
                search_result_dict["question"] = question_title
                search_result_dict["description"] = question_description
                search_result_dict["replies"] = replies
                
                response.append(search_result_dict)
            return jsonify(response)
    else:
        {'error':'no matches found'},500