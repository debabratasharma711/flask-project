"""module contains all the functions for frontend interaction"""
import requests, json
from flask import render_template, redirect, flash, request, url_for, jsonify, make_response
from application import app
from application.forms import LoginForm, SignUpForm, QuestionForm, ModifyForm, ChangePwdForm, AnswerForm, ModifyAnsForm
from flask_jwt_extended import set_access_cookies, unset_jwt_cookies, jwt_required, verify_jwt_in_request_optional, get_jwt_identity


DOMAIN = "http://127.0.0.1:5000"

def get_user_identity():
    """returns user identity
    type: int"""
    try:
        verify_jwt_in_request_optional()
        return get_jwt_identity()
    except:
        pass
    
def get_all_questions():
    """returns list of questions
    return type: list
    """
    url = DOMAIN + url_for('get_questions')
    response = requests.get(url)
    response_list = []
    if response.status_code==200:
        response = requests.get(url).json()
        for key, values in response.items():
            response_list.append(values)
        return response_list
    else:
        return "No questions"

@app.route('/')
@app.route('/home')
def homepage():
    """
    Url for homepage function.

    :http relative-url: '/','/home'
    """
    questions = get_all_questions()
    current_id = get_user_identity()
    if current_id == None:
        return render_template('home.html',title='Home', questions=questions, user_id=current_id)
    return redirect(url_for('user_home'))

@app.route('/signin', methods=['GET','POST'])
def loginform():
    """
    Url for login function.

    :http relative-url: /signin
    :http-method :GET, POST
    """
    form = LoginForm()
    if form.validate_on_submit():
        url = DOMAIN + url_for('login')
        payload = dict()
        payload['email'] = request.form['email']
        payload['password'] = request.form['password']
        headers = {'Content-type': 'application/json'}
        payload = json.dumps(payload)
        response = requests.request('POST', url, headers=headers, data=payload)
        flash('Login requested for email={}'.format(form.email.data))
        if response.status_code==200:
            current_id = get_user_identity()
            flash("Successful login")
            redirect_to = url_for('user_home')
            token = response.json()["token"]
            new_response = make_response(redirect(redirect_to,302))
            set_access_cookies(new_response, token)
            return new_response
        elif response.status_code==404:
            flash("Incorrect Email or Password")
            return render_template('login.html',form=form)

        else:
            flash("Internal server error")
            return render_template('login.html', form=LoginForm())
    return render_template('login.html', title='Sign In', form=form)

@app.route('/signup', methods=['GET', 'POST'])
def new_user():
    """
    Url to sign up account function.

    :http relative-url: /signup
    :http-method :GET, POST
    """
    form = SignUpForm()
    if form.validate_on_submit():
        if request.form['password']!=request.form['confirm_password']:
            flash("Password and Confirm Password don't match")
            return render_template('signup.html', title='SignUp', form=form)
        
        url = DOMAIN + url_for('register')
        payload = dict()
        payload['username'] = request.form['username']
        payload['email'] = request.form['email']
        payload['password'] = request.form['password']
        payload['confirm password'] = request.form['confirm_password']
        payload = json.dumps(payload)
        headers = {'Content-type': 'application/json'}
        response = requests.request('POST', url, headers=headers, data=payload)
        return redirect(DOMAIN+url_for('loginform'))
    else:
        return render_template('signup.html',title='SignUp', form=form)

@app.route('/user/logout', methods=['GET','POST'])
def user_logout():
    """
    Url to user logout function.

    :http relative-url: /user/logout
    :http-method :GET, POST
    """

    redirect_to = url_for('homepage')
    new_response = make_response(redirect(redirect_to))
    unset_jwt_cookies(new_response)
    return new_response, 200


@app.route('/user/<user_id>/delete', methods=['GET','DELETE'])
def delete_user_ac(user_id):
    """
    Url to delete user account function.

    :http relative-url: /<user_id>/delete
    :http-method :GET, DELETE
    """
    redirect_to = url_for('homepage')
    url_delete_user = DOMAIN + url_for('delete',userid=user_id)
    headers = dict(request.headers)
    headers.update({'Content-type': 'application/json'})
    delete_user = requests.request('DELETE', url_delete_user, headers=headers)
    if delete_user.status_code==200:
        new_response = make_response(redirect(redirect_to), 302)
        unset_jwt_cookies(new_response)
        flash("Account deleted")
        return new_response
    else:
        flash("Account does not exist")
        return redirect_to(redirect_to)

@app.route('/user/<user_id>/update', methods=['GET','POST'])
def modify_password(user_id):
    """
    Url modify password funnction.

    :http relative-url: /<user_id>/update
    :http-method :GET, POST
    """
    form = ChangePwdForm()
    current_id = get_user_identity()
    if form.validate_on_submit():
        update_pwd_url = DOMAIN + url_for('modify',userid=current_id)
        payload = dict()
        payload['old_password'] = request.form['old_password']
        payload['new_password'] = request.form['new_password']
        payload['confirm_password'] = request.form['confirm_password']
        payload = json.dumps(payload)
        header = dict(request.headers)
        header.update({'Content-type': 'application/json'})
        modify_request = requests.request('PUT', update_pwd_url, data=payload, headers=header)
        if modify_request.status_code==200:
            flash('modified successfully')
            return redirect(url_for('user_home'))
        else:
            flash("check password")
            return render_template('modify_user.html', form=form)

    return render_template('modify_user.html', form=form)

def add_question(form, current_id):
    """
    
    """
    if form.validate_on_submit():
        payload = dict()
        payload['title'] = request.form['title']
        payload['description'] = request.form['description']
        payload = json.dumps(payload)
        headers = dict(request.headers)
        headers.update({
        'Content-Type': 'application/json'
        })
        print(headers)
        url_post_question = DOMAIN +url_for('post_question')
        response = requests.request('POST', url=url_post_question, headers=headers, data=payload)
        print(response.text)
        if response.status_code == 200:
            questions = get_all_questions()
            return questions

@app.route('/user-home',methods=['GET','POST'])
def user_home():
    """
    Url for user home funnction.

    :http relative-url: /user-home>
    :http-method :GET, POST
    """
    question_form = QuestionForm()
    headers = dict(request.headers)
    headers.update({'Content-type': 'application/json'})
    current_id = get_user_identity()
    url_user_questions = DOMAIN + url_for('get_question_by_uid',uid=current_id)
    url_user_answers = DOMAIN + url_for('api_get_answer_by_uid', user_id=current_id)
    response_answer_uid = requests.request('GET', url_user_answers, headers=headers)
    response_questions_qid = requests.request('GET',url_user_questions)
    user_question = response_questions_qid.json()
    user_answer = response_answer_uid.json()
    all_questions = get_all_questions()
    if request.method == 'GET':
        return render_template('user-home.html', title="User Home", user_questions=user_question, all_questions=all_questions,
            all_answers=user_answer, form=question_form, userid=current_id)
    if question_form.validate_on_submit():
        get_added_question= add_question(question_form, current_id)
        return redirect(url_for('user_home'))
    flash("please fill out forms")
    return render_template('user-home.html',title="User Home",user_questions=user_question, all_questions=all_questions, all_answers=user_answer,
        form=question_form, userid=current_id)


@app.route('/users')
def get_users():
    """
    Get user function.

    :http relative-url: /users>
    :http-method :GET
    """

    url = DOMAIN + url_for('register')
    response = requests.request('GET', url = url)
    print(response.json())
    user_details=[]
    for each_user_id, each_user_data in response.json().items():
        user_details.append(each_user_data)
    return render_template('users.html',users=user_details)


@app.route('/user/<user_id>/question/<question_id>/modify', methods=['GET', 'POST', 'PUT'])
def modify_question(user_id, question_id):
    """
    Modify question function.

    :http relative-url: /user/<user_id>/question/<question_id>/modify'>
    :http-methods :'GET', 'POST', 'PUT'
    """

    form = ModifyForm()
    current_id = get_user_identity()
    url_question = DOMAIN + url_for('get_question_by_qid',qid=question_id)
    get_question = requests.request('GET', url=url_question)
    question_json = get_question.json()
    if form.validate_on_submit():
        url_update = DOMAIN + url_for('update_question', qid=question_id)
        payload = {}
        payload['title'] = request.form['title']
        payload['description'] = request.form['description']
        payload = json.dumps(payload)
        headers = dict(request.headers)
        headers.update({'Content-type': 'application/json'})
        modify_request = requests.request('PUT', url_update, data=payload, headers=headers)
        if modify_request.status_code==200:
            flash('modified successfully')
            return redirect(url_for('user_home'))
    return render_template('question-modify.html', form=form, question=question_json, user_id=current_id, question_id=question_id)


# def delete_question()
@app.route('/user/question/<q_id>/del', methods=['GET','POST','DELETE'])
def delete_question(q_id):
    """
    Delete question function.

    :http relative-url: /user/question/<q_id>/del'>
    :http-methods :'GET', 'POST', 'DELETE'
    """

    url_delete = DOMAIN + url_for('del_question',qid=q_id)
    headers = dict(request.headers)
    headers.update({'Content-type': 'application/json'})
    delete_question = requests.request('DELETE', url_delete, headers=headers)
    if delete_question.status_code==200:
            flash('deleted successfully')
    return redirect(url_for('user_home'))


@app.route('/user/question/<q_id>/answers', methods=['GET','POST'])
def get_answers_qid(q_id):
    """
    get answers by question id function.

    :http relative-url: /user/question/<q_id>/answers'>
    :http-methods :'GET', 'POST'
    """

    url_answers = DOMAIN + url_for('api_get_all_answers_qid', q_id=q_id)
    get_answers = requests.request('GET', url_answers)
    if get_answers.status_code == 200:
        get_answers = requests.request('GET', url_answers).json()
        return render_template('answers.html', answers=get_answers)
    elif get_answers.status_code == 404:
        flash("No answers posted")
        return(redirect(url_for('homepage')))


@app.route('/question/<q_id>/add_answer', methods=['GET','POST'])
def post_answer(q_id):
    """
    Post question function.

    :http relative-url: /question/<q_id>/add_answer'>
    :http-methods :'GET', 'POST'
    """

    form = AnswerForm()
    if form.validate_on_submit():
        url_post_answer = DOMAIN + url_for('api_post_answer', q_id=q_id)
        current_id = get_user_identity()
        headers = dict(request.headers)
        headers.update({'Content-type': 'application/json'})
        payload = dict()
        payload['answer_desc']=request.form['answer']
        payload = json.dumps(payload)
        response = requests.request('POST', url_post_answer,data= payload, headers=headers)
        if response.status_code==200:
            return redirect(url_for('user_home'))
        elif response.status_codes==302:
            return "unauthorized access"
    return render_template('post-ans.html',form=form)


@app.route('/answer/<answer_id>/modify', methods=['GET','POST','PUT'])
def modify_answer(answer_id):
    """
    Modify answer function.

    :http relative-url: /answer/<answer_id>/modify'>
    :http-methods :'GET', 'POST', 'PUT'
    """

    form = ModifyAnsForm()
    if form.validate_on_submit():

        url_modify_answer = DOMAIN + url_for('api_update_answer', answer_id=answer_id)
        payload = dict()
        payload['answer_desc'] = request.form['answer_desc']
        payload = json.dumps(payload)
        headers = dict(request.headers)
        headers.update({'Content-type': 'application/json'})
        response = requests.request('PUT', url_modify_answer, headers=headers, data = payload)

        if response.status_code==200:
            flash("Answer updated")
            return redirect(url_for('user_home'))
        elif response.status_code==302:
            flash("Unauthorized access")
            return redirect(url_for('user_home'))
        elif response.status_code==404:
            flash("Unauthorized access")
            return redirect(url_for('user_home'))
    return render_template('modify-answer.html', form=form)

@app.route('/answer/<answer_id>/delete', methods=['GET','POST','DELETE'])
def delete_answer(answer_id):
    """
    Delete answer function.

    :http relative-url: /answer/<answer_id>/delete'>
    :http-methods :'GET', 'POST', 'DELETE'
    """

    url_delete = DOMAIN + url_for('api_delete_answer', answer_id=answer_id)
    headers = dict(request.headers)
    headers.update({'Content-type': 'application/json'})
    delete_answer = requests.request('DELETE', url_delete, headers=headers)
    if delete_answer.status_code==200:
            flash('deleted successfully')
    elif delete_answer.status_code==401:
            flash('Unauthorized operation')
    return redirect(url_for('user_home'))

@app.route('/search', methods=['GET','POST'])
def search_post():
    """
    Search post function.

    :http relative-url: /search'>
    :http-methods :'GET', 'POST'
    """

    query = request.args.get('search')
    search_query = dict()
    search_query['query'] = query
    search_query = json.dumps(search_query)
    url_search = DOMAIN + url_for('api_search_question', search_query='search_query')
    headers = dict(request.headers)
    headers.update({'Content-type': 'application/json'})
    search_response = requests.request('GET', url_search, headers=headers, data=search_query)
    if search_response.status_code==200:
        search_response = search_response.json()
        return render_template('search-result.html', result = search_response)
    else:
        flash("No search results")
        return(redirect(url_for('homepage')))
