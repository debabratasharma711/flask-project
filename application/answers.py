#get answers by question id(to be used in home page to see answer)
#get answer by user id(i.e get answers post on your questions....to be used in YOUR ANSWERS division in front end)
#post answers
#edit answers
#delete answers

from flask import url_for, flash, redirect, request,abort, jsonify,Flask
from application import app, db, bcrypt,login_manager
from application.models import User, Question, Answers
from flask_login import login_user, current_user, logout_user, login_required
from flask_jwt_extended import create_access_token,jwt_required, get_jwt_identity
import datetime
import json
from application.search import get_data_to_add

#post an answer
@app.route('/question/<int:q_id>/answer', methods=['POST'])
@jwt_required
def api_post_answer(q_id):
    """
    Get answer by q id API endpoint function.
    :http relative-url: /question/<q_id>/answer
    :http-methods :'POST'
    """
    active_user_id = get_jwt_identity()
    active_user = User.query.filter(User.user_id==active_user_id).first()
    if not active_user:
        return {'error':'unauthorized access'}

    question_obj = Question.query.filter(Question.q_id==q_id).first()
    if not question_obj:
        return {"Question not found"}
    api_answer=request.json
    answer = Answers(answer_desc=api_answer["answer_desc"], question_id = q_id, user_id=active_user.user_id)
    db.session.add(answer)
    db.session.commit()
    get_data_to_add(app.config['INDEX'])
    return {'Message': 'Answer added'}, 200

@app.route('/question/<int:q_id>/answer', methods=['GET'])
def api_get_all_answers_qid(q_id):
    """
    Get answer by user id API endpoint function.
    :http relative-url: /question/<q_id>/answer
    :http-methods :'GET'
    """
    question_obj = Question.query.filter(Question.q_id==q_id).first()
    all_answers_obj = Answers.query.filter(Answers.question_id==q_id).all()
    if not all_answers_obj:
        return {"message": "No answers posted"}, 404
    answers_dict =  dict()
    if not question_obj:
        return {'error':'Post does not exist'}, 404
    for each_answer in all_answers_obj:
        if question_obj.title not in answers_dict.keys():
            answers_dict[question_obj.title] = dict()
            answers_dict[question_obj.title][each_answer.answer_id] = each_answer.answer_desc
        else:
            answers_dict[question_obj.title][each_answer.answer_id] = each_answer.answer_desc
            
    return jsonify(answers_dict)


@app.route('/user/<user_id>/answer', methods=['GET'])
@jwt_required
def api_get_answer_by_uid(user_id):
    """
    Get answer by user id API endpoint function.
    :http relative-url: /user/<user_id>/answer
    :http-methods :'GET'
    """

    active_user_id = get_jwt_identity()
    active_user = User.query.filter(User.user_id==active_user_id).first()
    if not active_user:
        return {'error':'unauthorized access'}, 302
    answer_obj = Answers.query.filter(Answers.user_id==active_user_id).all()
    if not answer_obj:
        return {'message': 'No answers posted'}, 404
    answers_dict = dict()
    for each_answer in answer_obj:
        question_obj = Question.query.filter(Question.q_id==each_answer.question_id).first()
        if question_obj:
            if question_obj.title not in answers_dict.keys():
                answers_dict[question_obj.title] = dict()
                answers_dict[question_obj.title][each_answer.answer_id] = each_answer.answer_desc
            else:
                answers_dict[question_obj.title][each_answer.answer_id] = each_answer.answer_desc
            
    if not answers_dict:
        return {'message': 'No answers posted'}, 404
    return jsonify(answers_dict)


# update answer
@app.route('/answer/<answer_id>', methods=['PUT'])
@jwt_required
def api_update_answer(answer_id):
    """
    Update answer API endpoint function.
    :http relative-url: /answer/answer_id'>
    :http-methods :'PUT'
    """

    active_user_id = get_jwt_identity()
    active_user = User.query.filter(User.user_id==active_user_id).first()
    if not active_user:
        return {'error':'unauthorized access'}, 302
    api_response = request.json
    answer_obj = Answers.query.filter(Answers.answer_id==answer_id).first()
    old_answer = answer_obj.answer_desc
    answer_obj.answer_desc = api_response['answer_desc']
    db.session.add(answer_obj)
    db.session.commit()
    get_data_to_add(app.config['INDEX'])
    new_answer = Answers.query.filter(Answers.answer_id==answer_id).first()
    return {old_answer:new_answer.answer_desc},200

@app.route('/answer/<answer_id>', methods=['DELETE'])
@jwt_required
def api_delete_answer(answer_id):
    """
    Delete answer API endpoint function.

    :http relative-url: /answer/answer_id'>
    :http-methods :'DELETE'
    """

    active_user_id = get_jwt_identity()
    active_user = User.query.filter(User.user_id==active_user_id).first()
    if not active_user:
        return {'error':'unauthorized access'}, 302
    answer_obj = Answers.query.filter(Answers.answer_id==answer_id).first()
    if answer_obj:
        db.session.delete(answer_obj)
        db.session.commit()
        get_data_to_add(app.config['INDEX'])
        return {'message':'answer deleted'}, 200
    else:
        return {'error':'no answer found'}, 404




