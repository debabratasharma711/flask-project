from flask import current_app
from application import app
from application.models import Question,Answers

def add_to_index(index, model_question, model_answer):
    with app.app_context():
        if not current_app.elasticsearch:
            return
        payload = {}
        for field in model_question.__searchable__:
            payload[field] = getattr(model_question, field)
        for answers in model_answer:
            for field in answers.__searchable__:
                if field not in payload.keys():
                    payload[field]=[] 
                    payload[field].append(getattr(answers, field))
                else:
                    payload[field].append(getattr(answers, field))

        current_app.elasticsearch.index(index=index, id=model_question.q_id, body=payload)

def get_data_to_add(index):
    for question in Question.query.all():
        answers =  Answers.query.filter(Answers.question_id==question.q_id).all()
        add_to_index(index, question, answers)

def remove_from_index(index, model):
    with app.app_context():
        if not current_app.elasticsearch:
            return
        current_app.elasticsearch.delete(index=index, id=model.q_id)

def query_index(index, query, page, per_page):
    with app.app_context():
        if not current_app.elasticsearch:
            return [], 0
        search = current_app.elasticsearch.search(
            index=index,
            body={'query': {'multi_match': {'query': query, 'fields': ['*']}},
                'from': (page - 1) * per_page, 'size': per_page})
        ids = [int(hit['_id']) for hit in search['hits']['hits']]
        return ids, search['hits']['total']['value']