from flask import Flask, render_template, url_for, redirect
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_jwt_extended import JWTManager
from elasticsearch import Elasticsearch
from celery import Celery
from celery.schedules import crontab


app = Flask(__name__)
app.config.from_pyfile('config.ini')
app.elasticsearch = Elasticsearch([app.config['ELASTICSEARCH_URL']]) \
        if app.config['ELASTICSEARCH_URL'] else None
db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
jwt = JWTManager(app)
blacklist = set()
login_manager = LoginManager()
login_manager = LoginManager(app)
login_manager.login_view = 'login'
login_manager.login_message_category = 'info'
celery = Celery(app.name)
celery.conf.update({
    'broker_url': 'redis://localhost:6379/0',
    'result_backend': 'redis://localhost:6379/0',
    "five-minute-task": {
            "task": 'tasks.refresh_celery',
            "schedule": crontab(minute='*/3'),
            "args": ['covid_daily_data']
        }
    })
from application import routes
from application import questions
from application import answers
from application import views