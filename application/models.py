"""this module defines all the tables in the db"""

from application import db


class User(db.Model):
    """user table"""
    user_id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password =  db.Column(db.String(60), nullable=False)
    
class Question(db.Model):
    """question table"""
    __searchable__ = ['title', 'description']
    q_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String(100), nullable=False)
    description = db.Column(db.Text, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.user_id'), nullable=False)
    answer = db.relationship('Answers', backref='author', lazy='dynamic', cascade="all, delete-orphan")
    
class Answers(db.Model):
    """ answers table"""
    __searchable__=['answer_desc']
    answer_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    answer_desc = db.Column(db.Text, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.user_id'), nullable=False)
    question_id = db.Column(db.Integer, db.ForeignKey('question.q_id'), nullable=False)
